import os
from celery import Celery
import time
from celery import shared_task

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'django_cookbook.settings')

app = Celery('django_cookbook')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))


@shared_task(queue='high')
def wait_task_high(time_delay=10):
    time.sleep(time_delay)
    return 'high_task_completed'


@shared_task(queue='default')
def wait_task_default(time_delay=10):
    time.sleep(time_delay)
    return 'default_task_completed'


@shared_task(queue='low')
def wait_task_low(time_delay=10):
    time.sleep(time_delay)
    return 'lower_task_completed'
