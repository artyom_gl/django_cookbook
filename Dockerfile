FROM ubuntu:18.04
# Install system dependencies
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y openssl curl git python3 python3-pip python3-dev python-dev \
 gettext libxml2-dev libxslt1-dev libmysqlclient-dev mysql-client libpq-dev libjpeg-dev libffi-dev \
 libcairo2 libpango1.0-dev libfreetype6-dev unzip gettext zip vim graphicsmagick ghostscript \
 libmemcached-dev xvfb wget binutils libproj-dev gdal-bin build-essential libssl-dev locales \
 tzdata libldap2-dev libsasl2-dev zlib1g-dev python-pip librtmp-dev libcurl4 libcurl4-openssl-dev

COPY . /data
WORKDIR /data

# Django specific commands
ENV DJANGO_SETTINGS_MODULE=django_cookboook.settings
RUN python3 -m pip install -U --no-cache-dir -r requirements.txt
