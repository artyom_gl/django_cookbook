# Generated by Django 2.2.2 on 2019-06-11 19:44

import django.contrib.postgres.fields.jsonb
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('entities', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='hero',
            name='features',
            field=django.contrib.postgres.fields.jsonb.JSONField(default=None),
        ),
    ]
