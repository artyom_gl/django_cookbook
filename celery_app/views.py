from django.contrib import messages
from django.views.generic.edit import FormView
from django.shortcuts import redirect
from django.contrib.auth.models import User
from celery_app.forms import GenerateRandomUserForm
from django_cookbook.tasks import create_random_user_accounts
from django.views import View

from django_cookbook.celery import (wait_task_high, wait_task_default, wait_task_low)


class GenerateRandomUserView(FormView):
    template_name = 'generate_random_users.html'
    form_class = GenerateRandomUserForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['users_list'] = User.objects.all()[:500]
        context['users_counter'] = User.objects.count()
        return context

    def form_valid(self, form):
        total = form.cleaned_data.get('total')
        create_random_user_accounts.delay(total)
        messages.success(self.request, 'We are generating your random users! Wait a moment and refresh this page.')
        return redirect('generate_users')


class CreateHighTask(View):

    def get(self, request):
        asd = wait_task_high.apply_async(queue='default', kwargs={'time_delay': 5})

        return redirect('generate_users')


class CreateDefaultTask(View):

    def get(self, request):
        for i in range(1, 10):
            wait_task_default.delay(time_delay=5)
        return redirect('generate_users')


class CreateLowTask(View):

    def get(self, request):
        for i in range(1, 10):
            wait_task_low.delay(time_delay=5)
        return redirect('generate_users')


class UserListRemove(View):

    def get(self, request):
        User.objects.all().exclude(id=1).delete()
        return redirect('generate_users')
