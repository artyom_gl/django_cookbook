from django.urls import path
from django.contrib.auth.decorators import login_required, permission_required

from .views import *
from django.contrib.auth.decorators import login_required

urlpatterns = [

    path('generate_users', login_required(GenerateRandomUserView.as_view(),
                                          login_url='/admin/login'),
         name='generate_users'),
    path('users_list_remove', login_required(UserListRemove.as_view(),
                                             login_url='/admin/login'),
         name='users_list_remove'),

    path('high_task', CreateHighTask.as_view(), name='high_task'),
    path('default_task', CreateDefaultTask.as_view(), name='default_task'),
    path('low_task', CreateLowTask.as_view(), name='low_task'),
]
