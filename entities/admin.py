# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.apps import apps
from django.contrib.auth.models import User, Group, Permission, ContentType
from django.contrib.admin.models import LogEntry
from django.contrib.sessions.models import Session
from entities.models import Origin


@admin.register(Origin)
class OriginAdmin(admin.ModelAdmin):
    list_display = ("name", "hero_count", "villain_count")

    def hero_count(self, obj):
        return obj.hero_set.count()

    def villain_count(self, obj):
        return obj.villain_set.count()


models = apps.get_models()

for model in models:
    try:
        admin.site.register(model)
    except admin.sites.AlreadyRegistered:
        pass

admin.site.unregister(User)
admin.site.unregister(Group)
admin.site.unregister(Permission)
admin.site.unregister(ContentType)
admin.site.unregister(Session)
admin.site.unregister(LogEntry)

admin.site.site_header = "UMSRA Admin"
admin.site.site_title = "UMSRA Admin Portal"
admin.site.index_title = "UMSRA Admin Portal"
